#### Binary classification of gym memberships

- [notebook preview](https://micstan.gitlab.io/share_zone/gym_modeling.html)
- binary classification of gym subscriptions
- Json preprocessing, scikit-learn pipelines, model and parameters selection, validation with [yellowbrick](https://www.scikit-yb.org/en/latest/).
